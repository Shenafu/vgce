//=============================================================================
// VGCE.js
//=============================================================================

/*:
 * @plugindesc Virtual Grid Combat Engine
 * $Amuseum_VGCE
 * @author Amuseum
 *
 * @param Enemy Sprite Size
 * @desc Same for width and height
 * Default 128
 * @default 128
 *
 * @param Max Enemy Grid Row
 * @desc Number of rows in the enemy grid
 * Default 3
 * @default 3
 *
 * @param Max Enemy Grid Column
 * @desc Number of columns in the enemy grid
 * Default 3
 * @default 3
 *
 * @param Enemy Grid X
 * @desc Enemy grid start X
 * Default Graphics.boxWidth >> 3
 * @default Graphics.boxWidth >> 3
 *
 * @param Enemy Grid Y
 * @desc Enemy grid start Y
 * Default (Graphics.boxHeight - (enemySpriteSize<<1) - (enemySpriteSize>>1)) >> 1
 * @default (Graphics.boxHeight - (enemySpriteSize<<1) - (enemySpriteSize>>1)) >> 1
 *
 * @param Enemy Grid DX
 * @desc X distance between enemies
 * Default 192
 * @default 192
 *
 * @param Enemy Grid DY
 * @desc Y distance between enemies
 * Default 128
 * @default 128
 *
 * @param Max Battle Members
 * @desc Active party members in combat
 * Default 4
 * @default 4
 *
 * @param Max Actor Grid Rows
 * @desc Number of rows in the actor grid
 * Default 2
 * @default 2
 *
 * @param Max Actor Grid Columns
 * @desc Number of columns in the actor grid
 * Default 2
 * @default 2
 *
 * @param Actor Grid X
 * @desc Actor grid start X
 * Default Graphics.boxWidth - (Graphics.boxWidth>>2) - (Graphics.boxWidth>>4)
 * @default Graphics.boxWidth - (Graphics.boxWidth>>2) - (Graphics.boxWidth>>4)
 *
 * @param Actor Grid Y
 * @desc Actor grid start Y
 * Default (Graphics.boxHeight>>1)
 * @default (Graphics.boxHeight>>1)
 *
 * @param Actor Grid DX
 * @desc X distance between actors
 * Default 96
 * @default 96
 *
 * @param Actor Grid DY
 * @desc Y distance between actors
 * Default 96
 * @default 96
 *
 * @param Battle Stats Font Size
 * @desc Font size of battle scene stats
 * Default 16
 * @default 16
 *
 * @param Status Icon Scale
 * @desc Scale dimensions of battle status icons
 * Default 0.75
 * @default 0.75
 *
 * @param Vertical Gauge Width
 * @desc Width of vertical gauges
 * Default 16
 * @default 16
 *
 * @param Vertical Gauge Length
 * @desc Length of vertical gauges
 * Default 36
 * @default 36
 *
 * @param Horizontal Gauge Width
 * @desc Width of horizontal gauges
 * Default 16
 * @default 16
 *
 * @param Horizontal Gauge Length
 * @desc Length of horizontal gauges
 * Default 116
 * @default 116
 *
 * @param Gauge Back Color
 * @desc rgba(R, G, B, A)
 * Default rgba(255, 255, 255, .8)
 * @default rgba(255, 255, 255, .8)
 *
 * @param HP Color 1
 * @desc Use system colors
 * Default 18
 * @default 18
 *
 * @param HP Color 2
 * @desc Use system colors
 * Default 3
 * @default 3
 *
 * @param MP Color 1
 * @desc Use system colors
 * Default 18
 * @default 18
 *
 * @param MP Color 2
 * @desc Use system colors
 * Default 1
 * @default 1
 *
 * @param TP Color 1
 * @desc Use system colors
 * Default 6
 * @default 6
 *
 * @param TP Color 2
 * @desc Use system colors
 * Default 18
 * @default 18
 *
 * @param Menu Command Columns
 * @desc Menu command columns
 * Default 8
 * @default 8
 *
 * @param Menu Command Rows
 * @desc Menu command rows
 * Default 2
 * @default 2
 *
 * @help This plugin does not provide plugin commands.
 *
 * Change scope of a skill or weapon.
 * <scope:row> attacks an entire row.
 * <scope:col> attacks an entire column.
 * <scope:x> attacks enemies in the center and all corners.
 * <scope:t> attacks enemies in the center and all edges.
 */

(function() {

/////////////////////////////////////////////////
// Plugin parameters
/////////////////////////////////////////////////

	var parameters = $plugins.filter(function(p) { return p.description.contains('$Amuseum_VGCE'); })[0].parameters;
	var maxEnemyGridRows = Number(parameters['Max Enemy Grid Rows']) || 3;
	var maxEnemyGridColumns = Number(parameters['Max Enemy Grid Columns']) || 3;
	var enemyGridX = String(parameters['Enemy Grid X']);
	var enemyGridY = String(parameters['Enemy Grid Y']);
	var enemyGridDX = Number(parameters['Enemy Grid DX']) || 192;
	var enemyGridDY = Number(parameters['Enemy Grid DY']) || 128;
	var maxBattleMembers = Number(parameters['Max Battle Members']) || 4;
	var maxActorGridRows = Number(parameters['Max Actor Grid Rows']) || 2;
	var maxActorGridColumns = Number(parameters['Max Actor Grid Columns']) || 2;
	var actorGridX = String(parameters['Actor Grid X']);
	var actorGridY = String(parameters['Actor Grid Y']);
	var actorGridDX = Number(parameters['Actor Grid DX']) || 96;
	var actorGridDY = Number(parameters['Actor Grid DY']) || 96;
	var battleStatsFontSize = Number(parameters['Battle Stats Font Size']) || 16;
	var statusIconScale = Number(parameters['Status Icon Scale']) || 0.75;
	var verticalGaugeWidth = Number(parameters['Vertical Gauge Width']) || 16;
	var horizontalGaugeWidth = Number(parameters['Horizontal Gauge Width']) || 16;
	var verticalGaugeLength = Number(parameters['Vertical Gauge Length']) || 16;
	var horizontalGaugeLength = Number(parameters['Horizontal Gauge Length']) || 16;
	var gaugeBackColor = String(parameters['Gauge Back Color']) || 'rgba(255, 255, 255, .8)';
	var hpGaugeColor1 = Number(parameters['HP Color 1']) || 18;
	var hpGaugeColor2 = Number(parameters['HP Color 2']) || 3;
	var mpGaugeColor1 = Number(parameters['MP Color 1']) || 18;
	var mpGaugeColor2 = Number(parameters['MP Color 2']) || 1;
	var tpGaugeColor1 = Number(parameters['TP Color 1']) || 6;
	var tpGaugeColor2 = Number(parameters['TP Color 2']) || 18;
	var enemySpriteSize = Number(parameters['Enemy Sprite Size']) || 18;
	var menuCommandColumns = Number(parameters['Menu Command Columns']) || 8;
	var menuCommandRows = Number(parameters['Menu Command Rows']) || 2;

	var maxEnemyGrids = maxEnemyGridRows * maxEnemyGridColumns;
	var maxActorGrids = maxEnemyGridRows * maxEnemyGridColumns;

/////////////////////////////////////////////////
// Game constants
/////////////////////////////////////////////////

TextManager.formation = 'Formation';

/////////////////////////////////////////////////
// Utility functions
/////////////////////////////////////////////////

Utils.gradientRatedColor = function(color1, color2, rate) {
	// colors should be in format #hhhhhh , where h are hex digits
	var r1 = parseInt(color1.substring(1,3), 16);
	var g1 = parseInt(color1.substring(3,5), 16);
	var b1 = parseInt(color1.substring(5,7), 16);
	var r2 = parseInt(color2.substring(1,3), 16);
	var g2 = parseInt(color2.substring(3,5), 16);
	var b2 = parseInt(color2.substring(5,7), 16);

	var r3 = r1 - (r1 - r2) * (1 - rate);
	var g3 = g1 - (g1 - g2) * (1 - rate);
	var b3 = b1 - (b1 - b2) * (1 - rate);

	return Utils.rgbToCssColor(r3, g3, b3);
};

Array.prototype.shuffle = Array.prototype.shuffle || function(maxlength) {
	// Fisher-Yates shuffle @ http://lambdaphant.com/blog/blackjack-in-javascript-part-I
	for (var j = maxlength || this.length, k, temp; --j; ) {
		k = ~~(j * Math.random()); // floor
		temp = this[j];
		this[j] = this[k];
		this[k] = temp;
	}
	return this;
};

Array.prototype.copyreverse = Array.prototype.copyreverse || function() {
	var b = [];
	for (var i=0, a=this, s=a.length-1; i<=s; i++) {
		b[i] = a[s-i];
	}
	return b;
};

Array.prototype.copy = Array.prototype.copy || function() {
	var b = [];
	for (var i=0, a=this, s=a.length; i<s; i++) {
		b[i] = a[i];
	}
	return b;
};

/////////////////////////////////////////////////
// Game_Battler
/////////////////////////////////////////////////

var oldGameBattlerBase_initMembers = Game_BattlerBase.prototype.initMembers;
Game_BattlerBase.prototype.initMembers = function() {
	oldGameBattlerBase_initMembers.call(this);

	this._gridIndex = 0;
};

Game_BattlerBase.prototype.gridCheckered = function(columns) {
	return ((this.gridIndex()%columns) ^ ~~(this.gridIndex()/columns)) & 1;
};

Game_BattlerBase.prototype.gridRow = function(columns) {
	return ~~(this.gridIndex()/columns);
};

Game_BattlerBase.prototype.gridColumn = function(columns) {
	return this.gridIndex()%columns;
};

Game_BattlerBase.prototype.setGridIndex = function(index) {
	return (this._gridIndex = index);
};

Game_BattlerBase.prototype.gridIndex = function() {
	return this._gridIndex;
};

Game_BattlerBase.sortByGridIndex = function(a, b) {
	if (a.gridIndex() > b.gridIndex()) {
		return 1;
	}
	if (a.gridIndex() < b.gridIndex()) {
		return -1;
	}
	return 0;
};

/////////////////////////////////////////////////
// Game_Actor
/////////////////////////////////////////////////

var oldGameActor_initMembers = Game_Actor.prototype.initMembers;
Game_Actor.prototype.initMembers = function() {
	oldGameActor_initMembers.call(this);

	this._gridIndex = 0;
};

// setup runs only on new games
var oldGameActor_setup = Game_Actor.prototype.setup;
Game_Actor.prototype.setup = function(actorId) {
	oldGameActor_setup.apply(this, arguments);

	this.initGridIndex(actorId);
};

Game_Actor.prototype.initGridIndex = function(actorId) {
	var actor = this.actor(actorId);
	if (actor._gridIndex == undefined) {
		actor._gridIndex = actorId - 1;
		//alert(actor._gridIndex);
	}
	this._gridIndex = actor._gridIndex;
};

Game_Actor.prototype.setGridIndex = function(index) {
	return (this.actor(index)._gridIndex = Game_BattlerBase.prototype.setGridIndex.apply(this, arguments));
};

//*
Game_Actor.prototype.isSpriteVisible = function() {
	// make actor visible when not in side-view
	return true;
};
//*/

/////////////////////////////////////////////////
// Game_Party
/////////////////////////////////////////////////

Game_Party.prototype.maxBattleMembers = function() {
	return maxBattleMembers;
};

Game_Party.prototype.battleMembers = function() {
	var list = [];
	for (var i=0, p=this.allMembers().reverse(), l=this.maxBattleMembers(), m; i<l && (m=p.pop()); i++) {
		m.gridIndex() < l && list.push(m);
	}
	return list;
};

Game_Party.prototype.highestGridIndex = function() {
	var highest = 0;

	for (var i=0, p=$gameParty.allMembers(), l=p.length; i<l; i++) {
		var gridIndex = p[i].gridIndex();
		if (gridIndex>highest) {
			highest = gridIndex;
		}
	}

	return highest;
};

var oldGameParty_addActor = Game_Party.prototype.addActor;
Game_Party.prototype.addActor = function(actorId) {
	if (!this._actors.contains(actorId)) {
		$gameActors.actor(actorId).setGridIndex(this.firstAvailableGridIndex());
	}

	oldGameParty_addActor.apply(this, arguments);
	$gameParty.sortByGridIndex();
};

Game_Party.prototype.firstAvailableGridIndex = function() {
	var used = [];
	var lowest = 0;

	for (var p=this.allMembers().copyreverse(), i; i=p.pop(); ) {
		used.push(i.gridIndex());
	}

	for (var i, u=used.sort().copyreverse(); (i=u.pop())>=0 && (i==lowest); lowest=i+1) {
	}

	return lowest;
};

Game_Party.prototype.gridIndexActor = function(gridIndex) {
	for (var i=0, p=this.allMembers(), l=p.length; i<l; i++) {
		if (p[i].gridIndex()==gridIndex) {
			return p[i];
		}
	}
};

var oldGameParty_swapOrder = Game_Party.prototype.swapOrder;
Game_Party.prototype.swapOrder = function(index1, index2) {
	this.swapGridIndex($gameActors.actor(index1), $gameActors.actor(index2));
	this.sortByGridIndex();
	$gamePlayer.refresh();
};

Game_Party.prototype.swapGridIndex = function(actor1, actor2) {
	var temp = actor1.gridIndex();
	actor1.setGridIndex(actor2.gridIndex());
	actor2.setGridIndex(temp);
};

Game_Party.prototype.sortByGridIndex = function() {
	var members = this.members().sort(Game_BattlerBase.sortByGridIndex);
	var actors = [];
	for (var i=0, m=members, l=m.length; i<l; i++) {
		actors[i] = m[i].actorId();
	}
	this._actors = actors;
};

/////////////////////////////////////////////////
// Game_Troop
/////////////////////////////////////////////////

Game_Troop.prototype.setup = function(troopId) {
	this.clear();
	var startx = eval(enemyGridX);
	var starty = eval(enemyGridY);
	this._troopId = troopId;
	while (!this._enemies.length && (this._enemies=[]) ) {
		for (var i=0, a=this.troop().members.shuffle(maxEnemyGrids), s=a.length; i<s; i++) {
			var member = a[i];
			if (member && $dataEnemies[member.enemyId] && Math.randomInt(5) >= 1 ) {
				var enemyId = member.enemyId;
				var x = startx + (i% maxEnemyGridColumns) * enemyGridDX;
				var y = starty + (~~(i/ maxEnemyGridColumns)) * enemyGridDY;
				var enemy = new Game_Enemy(enemyId, x, y);
				enemy.setGridIndex(i);

				if (member.hidden) {
					enemy.hide();
				}
				this._enemies.push(enemy);
			}
		}
	}
	this.makeUniqueNames();
};

/////////////////////////////////////////////////
// Game_Action
/////////////////////////////////////////////////

Game_Action.SCOPE_COL         = 41;
Game_Action.SCOPE_ROW        = 42;
Game_Action.SCOPE_X         = 43;
Game_Action.SCOPE_T         = 44;

Game_Action.prototype.checkItemScope = function(list) {
	var scope = this.item().meta.scope ? Game_Action['SCOPE_' + this.item().meta.scope.toUpperCase()] : this.item().scope;
	//alert(scope);
	//return list.contains(this.item().scope);
	return list.contains(scope);
};

Game_Action.prototype.isScopeCol = function() {
    return this.checkItemScope([Game_Action.SCOPE_COL]);
};
Game_Action.prototype.isScopeRow = function() {
    return this.checkItemScope([Game_Action.SCOPE_ROW]);
};
Game_Action.prototype.isScopeX = function() {
    return this.checkItemScope([Game_Action.SCOPE_X]);
};
Game_Action.prototype.isScopeT = function() {
    return this.checkItemScope([Game_Action.SCOPE_T]);
};

Game_Action.prototype.isForOpponent = function() {
    return this.checkItemScope([1, 2, 3, 4, 5, 6, 41, 42, 43, 44]);
};

Game_Action.prototype.targetsForOpponents = function() {
	var targets = [];
	var unit = this.opponentsUnit();

	// Amuseum changes start
	var member;
	if (this.isScopeCol()) {
		var mod = unit.members()[this._targetIndex].gridIndex() % maxEnemyGridColumns
		for (var i=0; i<maxEnemyGrids; i++) {
			member = unit.members()[i];
			member && member.gridIndex()%maxEnemyGridColumns == mod && member.isAlive() && targets.push(member);
		}
	} else if (this.isScopeRow()) {
		var mod = ~~(unit.members()[this._targetIndex].gridIndex() / maxEnemyGridColumns);
		for (var i=0; i<maxEnemyGrids; i++) {
			member = unit.members()[i];
			member && ~~(member.gridIndex()/maxEnemyGridColumns) == mod && member.isAlive() && targets.push(member);
		}
	} else if (this.isScopeX()) {
		var targetChecker = unit.members()[this._targetIndex].gridCheckered(maxEnemyGridColumns);
		for (var i=0; i<maxEnemyGrids; i++) {
			member = unit.members()[i];
			member && member.isAlive() && (member.gridCheckered(maxEnemyGridColumns)==targetChecker) && targets.push(member);
		}
	} else if (this.isScopeT()) {
		var target = unit.members()[this._targetIndex];
		var targetColumn = target.gridColumn(maxEnemyGridColumns);
		var targetRow = target.gridRow(maxEnemyGridColumns);
		for (var i=0; i<maxEnemyGrids; i++) {
			member = unit.members()[i];
			member && member.isAlive() && (member.gridColumn(maxEnemyGridColumns)==targetColumn || member.gridRow(maxEnemyGridColumns)==targetRow) && targets.push(member);
		}
	// Amuseum changes end

	} else if (this.isForRandom()) {
		for (var i = 0; i < this.numTargets(); i++) {
			targets.push(unit.randomTarget());
		}
	} else if (this.isForOne()) {
		if (this._targetIndex < 0) {
			targets.push(unit.randomTarget());
		} else {
			targets.push(unit.smoothTarget(this._targetIndex));
		}
	} else {
		targets = unit.aliveMembers();
	}

	return targets;
};

var oldGameActionNeedsSelection = Game_Action.prototype.needsSelection;
Game_Action.prototype.needsSelection = function() {
    if ($gameParty.inBattle() && this.checkItemScope([0])) return false;
    if ($gameParty.inBattle() && BattleManager.isForceSelection()) return true;
    return oldGameActionNeedsSelection.call(this);
};

/////////////////////////////////////////////////
// Sprite_Actor
/////////////////////////////////////////////////

Sprite_Actor.prototype.setBattler = function(battler) {
	Sprite_Battler.prototype.setBattler.call(this, battler);
	var changed = (battler !== this._actor);
	if (changed) {
		this._actor = battler;
		if (battler) {
			this.setActorHome(battler.index(), battler.gridIndex());
		}
		this.startEntryMotion();
		this._stateSprite.setup(battler);
	}
};

Sprite_Actor.prototype.setActorHome = function(index, gridIndex) {
	if ($gameSystem.isSideView()) {
		gridIndex = gridIndex || index;
		var homeX = eval(actorGridX) + (gridIndex%maxActorGridColumns) * actorGridDX;
		var homeY = eval(actorGridY) + ~~(gridIndex/maxActorGridColumns) * actorGridDY;
	} else {
		var homeX = Graphics.boxWidth / 8 + Graphics.boxWidth / 4 * index;
		var homeY = Graphics.boxHeight - 180;
	}
	this.setHome(homeX, homeY);
};

/////////////////////////////////////////////////
// Sprite_Enemy
/////////////////////////////////////////////////

Sprite_Enemy.prototype.initMembers = function() {
	Sprite_Battler.prototype.initMembers.call(this);
	this._enemy = null;
	this._appeared = false;
	this._battlerName = '';
	this._battlerHue = 0;
	this._effectType = null;
	this._effectDuration = 0;
	this._shake = 0;
	// Amuseum changes start
	this.width = enemySpriteSize;
	this.height = enemySpriteSize;
	// Amuseum changes end
	this.createStateIconSprite();
};

/////////////////////////////////////////////////
// Scene_Battle
/////////////////////////////////////////////////

Scene_Battle.prototype.updateWindowPositions = function() {
	/*
    var statusX = 0;
    if (BattleManager.isInputting()) {
        statusX = this._partyCommandWindow.width;
    } else {
        statusX = this._partyCommandWindow.width / 2;
    }
    if (this._statusWindow.x < statusX) {
        this._statusWindow.x += 16;
        if (this._statusWindow.x > statusX) {
            this._statusWindow.x = statusX;
        }
    }
    if (this._statusWindow.x > statusX) {
        this._statusWindow.x -= 16;
        if (this._statusWindow.x < statusX) {
            this._statusWindow.x = statusX;
        }
    }
    //*/
};

/////////////////////////////////////////////////
// Scene_Menu
/////////////////////////////////////////////////

Scene_Menu.prototype.commandFormation = function() {
	SceneManager.push(Scene_FormationGrid);
};

/////////////////////////////////////////////////
// Scene_FormationGrid
/////////////////////////////////////////////////

function Scene_FormationGrid() {
    this.initialize.apply(this, arguments);
}

Scene_FormationGrid.prototype = Object.create(Scene_MenuBase.prototype);
Scene_FormationGrid.prototype.constructor = Scene_FormationGrid;

Scene_FormationGrid.prototype.initialize = function() {
    Scene_MenuBase.prototype.initialize.call(this);
};

Scene_FormationGrid.prototype.create = function() {
	Scene_MenuBase.prototype.create.call(this);

	this.createHelpWindow();
	this.createFormationWindow();

	this._helpWindow.show();
	this._formationWindow.activate();
};

Scene_FormationGrid.prototype.createHelpWindow = function() {
    this._helpWindow = new Window_Help(1);
    this._helpWindow.setText(this.helpWindowText());
    this.addWindow(this._helpWindow);
};

Scene_FormationGrid.prototype.createFormationWindow = function() {
	var y = this._helpWindow.height;
	var width = Graphics.boxWidth;
	var height = Graphics.boxHeight - y;
	this._formationWindow = new Window_FormationGrid(0, y, width, height);
	this._formationWindow.setHandler('ok',     this.onFormationOk.bind(this));
	this._formationWindow.setHandler('cancel',		this.popScene.bind(this));
	this.addWindow(this._formationWindow);
};

Scene_FormationGrid.prototype.helpWindowText = function() {
    return TextManager.formation;
};

Scene_FormationGrid.prototype.onFormationOk = function() {
	var gridIndex = this._formationWindow.index();
	var currentActor = $gameParty.gridIndexActor(gridIndex);
	var pendingIndex = this._formationWindow.pendingIndex();
	if (pendingIndex >= 0) {
		var pendingActor = $gameParty.gridIndexActor(pendingIndex);
		if (currentActor && pendingActor) {
			$gameParty.swapOrder(currentActor.actorId(), pendingActor.actorId());
		}
		else {
			currentActor && currentActor.setGridIndex(pendingIndex);
			pendingActor && pendingActor.setGridIndex(gridIndex);
			$gameParty.sortByGridIndex();
		}

		this._formationWindow.setPendingIndex(-1);
		this._formationWindow.redrawCurrentItem();
		this._formationWindow.redrawItem(pendingIndex);
	} else {
		this._formationWindow.setPendingIndex(gridIndex);
	}
	this._formationWindow.activate();
	this._formationWindow.refresh();
};

/////////////////////////////////////////////////
// Window_Base
/////////////////////////////////////////////////

Window_Base.prototype.gaugeBackColor = function() {
		return gaugeBackColor;
};

Window_Base.prototype.hpGaugeColor1 = function() {
	return this.textColor(hpGaugeColor1);
};

Window_Base.prototype.hpGaugeColor2 = function() {
	return this.textColor(hpGaugeColor2);
};

Window_Base.prototype.mpGaugeColor1 = function() {
	return this.textColor(mpGaugeColor1);
};

Window_Base.prototype.mpGaugeColor2 = function() {
	return this.textColor(mpGaugeColor2);
};

Window_Base.prototype.tpGaugeColor1 = function() {
	return this.textColor(tpGaugeColor1);
};

Window_Base.prototype.tpGaugeColor2 = function() {
	return this.textColor(tpGaugeColor2);
};

// add scale factor
Window_Base.prototype.drawIcon = function(iconIndex, x, y, scale) {
	scale = scale || 1;
	var bitmap = ImageManager.loadSystem('IconSet');
	var pw = Window_Base._iconWidth;
	var ph = Window_Base._iconHeight;
	var sx = iconIndex % 16 * pw;
	var sy = ~~(iconIndex / 16) * ph;
	this.contents.blt(bitmap, sx, sy, pw, ph, x, y, pw * scale, ph * scale);
};

// add scale factor
Window_Base.prototype.drawActorIcons = function(actor, x, y, width, scale) {
	width = width || 144;
	scale = scale || 1;
	var icons = actor.allIcons().slice(0, ~~(width / Window_Base._iconWidth * scale));
	for (var i = 0; i < icons.length; i++) {
		this.drawIcon(icons[i], x + Window_Base._iconWidth * i * scale, y, scale);
	}
};

Window_Base.prototype.drawGauge = function(x, y, width, rate, color1, color2) {
	var frontColor = Utils.gradientRatedColor(color2, color1, rate);
	var fillW = ~~(width * rate);
	var gaugeY = y + ((this.lineHeight() - horizontalGaugeWidth) >> 1);
	this.contents.fillRect(x, gaugeY, fillW, horizontalGaugeWidth, frontColor);
	this.contents.fillRect(x + fillW, gaugeY, width - fillW, horizontalGaugeWidth, this.gaugeBackColor());
};

Window_Base.prototype.drawGaugeVertical = function(x, y, height, rate, color1, color2) {
	var frontColor = Utils.gradientRatedColor(color2, color1, rate);
	var fillTop = ~~(height * (1-rate));
	this.contents.fillRect(x, y, verticalGaugeWidth, fillTop, this.gaugeBackColor());
	this.contents.fillRect(x, y + fillTop, verticalGaugeWidth, height - fillTop, frontColor);
};

Window_Base.prototype.drawActorHpVertical = function(actor, x, y, width, height) {
	width = width || verticalGaugeWidth;
	height = height || this.height;
	var color1 = this.hpGaugeColor1();
	var color2 = this.hpGaugeColor2();
	this.drawGaugeVertical(x, y, height, actor.hpRate(), color1, color2);
	this.changeTextColor(this.systemColor());
	this.drawCurrentAndMax(actor.hp, actor.mhp, x, y + height*0.65, width, this.hpColor(actor), this.normalColor());
};

Window_Base.prototype.drawActorMpVertical = function(actor, x, y, width, height) {
	width = width || verticalGaugeWidth;
	height = height || this.height;
	var color1 = this.mpGaugeColor1();
	var color2 = this.mpGaugeColor2();
	this.drawGaugeVertical(x, y, height, actor.mpRate(), color1, color2);
	this.changeTextColor(this.systemColor());
	this.drawCurrentAndMax(actor.mp, actor.mmp, x, y + height*0.65, width, this.mpColor(actor), this.normalColor());
};

Window_Base.prototype.drawActorTpVertical = function(actor, x, y, width, height) {
	width = width || verticalGaugeWidth;
	height = height || this.height;
	var color1 = this.tpGaugeColor1();
	var color2 = this.tpGaugeColor2();
	this.drawGaugeVertical(x, y, height, actor.tpRate(), color1, color2);
	this.changeTextColor(this.systemColor());
	this.drawText(actor.tp, x, y + height*0.65, width, 'center');
};

/////////////////////////////////////////////////
// Window_PartyCommand
/////////////////////////////////////////////////

Window_PartyCommand.prototype.windowWidth = function() {
    return Graphics.boxWidth >> 1;
};

/////////////////////////////////////////////////
// Window_ActorCommand
/////////////////////////////////////////////////

Window_ActorCommand.prototype.windowWidth = function() {
	return Graphics.boxWidth >> 1;
};

Window_ActorCommand.prototype.maxCols = function() {
    return 2;
};

// list vertically instead of horiz
Window_ActorCommand.prototype.itemRect = function(index) {
	var rect = new Rectangle();
	var maxRows = this.numVisibleRows();
	rect.width = this.itemWidth();
	rect.height = this.itemHeight();
	rect.x = ~~(index / maxRows) * (rect.width + this.spacing()) - this._scrollX;
	rect.y = ~~(index % maxRows) * rect.height - this._scrollY;
	return rect;
};

/////////////////////////////////////////////////
// Window_BattleStatus
/////////////////////////////////////////////////

Window_BattleStatus.prototype.initialize = function() {
	var width = this.windowWidth();
	var height = this.windowHeight();
	var x = Graphics.boxWidth - width;
	var y = Graphics.boxHeight - height;
	Window_Selectable.prototype.initialize.call(this, x, y, width, height);
	this.addActorSprites();
	this.refresh();
	this.openness = 0;
};

Window_BattleStatus.prototype.windowWidth = function() {
	return Graphics.boxWidth >> 1;
};

Window_BattleStatus.prototype.addActorSprites = function() {
	// add party sprites with animations
};

// define party member's stats' screen position
// maxActorGridRows x maxActorGridColumns members

Window_BattleStatus.prototype.spacing = function() {
	return 0;
};

Window_BattleStatus.prototype.maxCols = function() {
	return maxActorGridColumns;
};

Window_BattleStatus.prototype.maxRows = function() {
	return maxActorGridRows;
};

Window_BattleStatus.prototype.itemHeight = function() {
	return 72;
};

Window_BattleStatus.prototype.itemWidth = function() {
	return ~~((this.width - this.padding * 2 + this.spacing()) / this.maxCols() - this.spacing());
};

Window_BattleStatus.prototype.maxItems = function() {
	return $gameParty.maxBattleMembers();
};

/*
Window_BattleStatus.prototype.itemRect = function(index) {
	var rect = new Rectangle();
	rect.width = this.width / maxActorGridColumns;
	rect.height = this.height / maxActorGridRows;
	rect.x = index % maxActorGridColumns * (rect.width + this.spacing()) - this._scrollX;
	rect.y = ~~(index / maxActorGridColumns) * rect.height - this._scrollY;

	return rect;

};
//*/

Window_BattleStatus.prototype.basicAreaRect = function(index) {
	var rect = this.itemRect(index);
	/*
	rect.width = rect.width / 2;
	rect.height = rect.height * 2;
	rect.x = (index % 2) * rect.width;
	rect.y = ~~(index / 2) * rect.height;
	//*/
	return rect;
};

// display party member's names and stats
Window_BattleStatus.prototype.drawBasicArea = function(rect, actor) {
	var icons = actor.allIcons().slice(0, ~~(rect.width / Window_Base._iconWidth * statusIconScale));
/*
//	this.drawActorName(actor, ( (actor.index() % 2 == 0) ? rect.x+rect.width-this.textWidth(actor.name())-verticalGaugeWidth-this.contents.fontSize : rect.x+verticalGaugeWidth+this.contents.fontSize ), rect.y-this.contents.fontSize*2/3, rect.width);
//	this.drawActorCharacter(actor, ( (actor.index() % 2 == 0) ? rect.x+rect.width-Window_Base._iconWidth*1.5 : rect.x+Window_Base._iconWidth*1.5 ), rect.y + rect.height - 4);
//	this.drawActorIcons(actor, ( (actor.index() % 2 == 0) ? rect.x+rect.width-icons.length*Window_Base._iconWidth*statusIconScale-Window_Base._iconWidth*1.5-verticalGaugeWidth*4-this.contents.fontSize - 8 : rect.x+Window_Base._iconWidth*1.5+verticalGaugeWidth*4+this.contents.fontSize ), rect.y + rect.height /2, rect.width, statusIconScale);
//*/
	this.drawActorName(actor, rect.x+4, rect.y-this.contents.fontSize*2/3, rect.width);
	this.drawActorCharacter(actor, rect.x+(Window_Base._iconWidth>>1)+(Window_Base._iconWidth>>2), rect.y+rect.height-4);
	this.drawActorIcons(actor, rect.x+4, rect.y+rect.height/2, rect.width, statusIconScale);

	// borders around box
/*
	(actor.index() % 2==0 && this.contents.fillRect(rect.x+rect.width-1, rect.y, 2, rect.height));
	(actor.index() / 2 < 1  && this.contents.fillRect(rect.x, rect.y+rect.height-1, rect.width, 2));
//*/
	this.contents.fillRect(rect.x+rect.width-1, rect.y, 2, rect.height);
	this.contents.fillRect(rect.x, rect.y+rect.height-2, rect.width, 2);
};

Window_BattleStatus.prototype.gaugeAreaRect = function(index) {
	var rect = this.itemRect(index);
	/*
	rect.width = rect.width / 2;
	rect.height = rect.height * 2;
	rect.x = (index % 2) * rect.width;
	rect.y = ~~(index / 2) * rect.height;
	//*/
	return rect;
};

Window_BattleStatus.prototype.drawItem = function(index) {
	var actor = $gameParty.gridIndexActor(index);
	if (actor) {
		this.drawGaugeArea(this.gaugeAreaRect(index), actor);
		this.drawBasicArea(this.basicAreaRect(index), actor);
	}
};

/*
Window_BattleStatus.prototype.drawGaugeAreaWithTp = function(rect, actor) {
	// vertical gauges
	this.contents.fontSize = battleStatsFontSize;
	this.drawGaugeAreaWithoutTp(rect, actor);
	this.drawActorTpVertical(actor, ( (actor.index() % 2 == 0) ? rect.x+rect.width-verticalGaugeWidth*3-this.contents.fontSize*1.5-Window_Base._iconWidth*1.5 : rect.x+this.contents.fontSize+Window_Base._iconWidth*1.5+verticalGaugeWidth*2 ), rect.y+20, verticalGaugeWidth, verticalGaugeLength);
};

Window_BattleStatus.prototype.drawGaugeAreaWithoutTp = function(rect, actor) {
	// vertical gauges
	this.contents.fontSize = battleStatsFontSize;
	this.drawActorHpVertical(actor, ( (actor.index() % 2 == 0) ? rect.x+rect.width-verticalGaugeWidth-this.contents.fontSize/2 : rect.x+this.contents.fontSize/2 ), rect.y+20, verticalGaugeWidth, verticalGaugeLength);
	this.drawActorMpVertical(actor, ( (actor.index() % 2 == 0) ? rect.x+rect.width-verticalGaugeWidth*2-this.contents.fontSize-Window_Base._iconWidth*1.5 : rect.x+this.contents.fontSize/2+Window_Base._iconWidth*1.5+verticalGaugeWidth ), rect.y+20, verticalGaugeWidth, verticalGaugeLength);
};
//*/

Window_BattleStatus.prototype.drawGaugeAreaWithTp = function(rect, actor) {
	// horizontal gauges
	this.contents.fontSize = battleStatsFontSize;
	this.drawGaugeAreaWithoutTp(rect, actor);
	this.drawActorTp(actor, rect.x+52, rect.y+40, rect.width - 60);
};

Window_BattleStatus.prototype.drawGaugeAreaWithoutTp = function(rect, actor) {
	// horizontal gauges
	this.contents.fontSize = battleStatsFontSize;
	this.drawActorHp(actor, rect.x+52, rect.y, rect.width - 60);
	this.drawActorMp(actor, rect.x+52, rect.y+20, rect.width - 60);
};

Window_BattleStatus.prototype.select = function(index) {
	var actor = $gameParty.gridIndexActor(index);
	if (actor) {
		Window_Selectable.prototype.select.call(this, index);
		return actor;
	}
};

Window_BattleStatus.prototype.updateCursor = function() {
	if (this._cursorAll) {
		var allRowsHeight = this.maxRows() * this.itemHeight();
		this.setCursorRect(0, 0, this.contents.width, allRowsHeight);
		this.setTopRow(0);
	} else if (this.isCursorVisible()) {
		var rect = this.basicAreaRect(this.index());
		this.setCursorRect(rect.x, rect.y, rect.width, rect.height);
	} else {
		this.setCursorRect(0, 0, 0, 0);
	}
};

Window_BattleStatus.prototype.hitTest = function(x, y) {
	if (this.isContentsArea(x, y)) {
		var cx = x - this.padding;
		var cy = y - this.padding;
		var topIndex = this.topIndex();
		for (var i = 0; i < this.maxPageItems(); i++) {
			var index = topIndex + i;
			if (index < this.maxItems()) {
				var rect = this.basicAreaRect(index);
				var right = rect.x + rect.width;
				var bottom = rect.y + rect.height;
				if (cx >= rect.x && cy >= rect.y && cx < right && cy < bottom) {
					return index;
				}
			}
		}
	}
	return -1;
};

/////////////////////////////////////////////////
// Window_BattleLog
/////////////////////////////////////////////////

Window_BattleLog.prototype.showNormalAnimation = function(targets,
animationId, mirror) {
    var animation = $dataAnimations[animationId];
    if (animation) {
			if (animation.position === 3) {
				targets.forEach(function(target) {
						target && target.startAnimation(animationId, mirror, 0);
				});
			} else {
					var delay = this.animationBaseDelay();
        	var nextDelay = this.animationNextDelay();
        	targets.forEach(function(target) {
            	target && target.startAnimation(animationId, mirror, delay);
            	delay += nextDelay;
        	});
			}
    }
};

/////////////////////////////////////////////////
// Window_BattleActor
/////////////////////////////////////////////////

Window_BattleActor.prototype.actor = function() {
	return $gameParty.gridIndexActor(this.index());
};

/////////////////////////////////////////////////
// Window_BattleEnemy
/////////////////////////////////////////////////

Window_BattleEnemy.prototype.sortTargets = function() {
	this._enemies.sort(Game_BattlerBase.sortByGridIndex);
};

Window_BattleEnemy.prototype.nameRect = function(index) {
	var rect = new Rectangle();
	if (!eval(Yanfly.Param.BECShowEnemyName)) return rect;
	var enemy = this._enemies[index];
	var maxCols = this.maxCols();
	this.contents.fontSize = Yanfly.Param.BECEnemyFontSize;
	var textSize = this.textWidth(' ' + enemy.name() + ' ');
	rect.width = Math.max(enemy.spriteWidth(), textSize);
	rect.height = this.lineHeight();
	var pad = this.standardPadding();
	rect.x = enemy.spritePosX() - rect.width / 2 - pad + 500;
	rect.y = enemy.spritePosY() - enemy.spriteHeight() - pad + 500;
	rect.y += enemy.spriteHeight() - this.lineHeight() -
	this.standardPadding();
	return rect;
};

Window_BattleEnemy.prototype.updateCursor = function() {
	if (eval(Yanfly.Param.BECEnemySelect)) {
		if (this._cursorAll) {
			var allRowsHeight = this.maxRows() * this.itemHeight();
			this.setCursorRect(0, 0, this.contents.width, allRowsHeight);
			this.setTopRow(0);
		} else if (this.index() < 0) {
			this.setCursorRect(0, 0, 0, 0);
		} else {
			this.ensureCursorVisible();
			var rect = this.nameRect(this.index());
			this.setCursorRect(rect.x, rect.y, rect.width, rect.height);
		}
	} else {
		Window_Selectable.prototype.updateCursor.call(this);
	}
};

/*
/////////////////////////////////////////////////
// Window_MenuCommand
/////////////////////////////////////////////////

Window_MenuCommand.prototype.windowWidth = function() {
	return Graphics.boxWidth;
};

Window_MenuCommand.prototype.windowHeight = function() {
	return this.fittingHeight(this.numVisibleRows());
};

Window_MenuCommand.prototype.numVisibleRows = function() {
	return menuCommandRows;
};

Window_MenuCommand.prototype.maxCols = function() {
	return menuCommandColumns;
};

/////////////////////////////////////////////////
// Window_MenuStatus
/////////////////////////////////////////////////

Window_MenuStatus.prototype.initialize = function(x, y, height) {
	this._height = height;
	var width = this.windowWidth();
	Window_Selectable.prototype.initialize.call(this, x, y, width, height);
	this._formationMode = false;
	this._pendingIndex = -1;
	this.loadImages();
	this.refresh();
};

Window_MenuStatus.prototype.windowWidth = function() {
	return Graphics.boxWidth;
};

Window_MenuStatus.prototype.windowHeight = function() {
	return this._height;
};

Window_MenuStatus.prototype.numVisibleRows = function() {
	return maxActorGridRows;
};

Window_MenuStatus.prototype.maxCols = function() {
	return maxActorGridColumns;
};
//*/

/////////////////////////////////////////////////
// Window_FormationGrid
/////////////////////////////////////////////////

function Window_FormationGrid() {
    this.initialize.apply(this, arguments);
}

Window_FormationGrid.prototype = Object.create(Window_Selectable.prototype);
Window_FormationGrid.prototype.constructor = Window_FormationGrid;

Window_FormationGrid.prototype.initialize = function(x, y) {
    var width = this.windowWidth();
    var height = this.windowHeight();
    Window_Selectable.prototype.initialize.call(this, x, y, width, height);
    this._pendingIndex = -1;
    this.refresh();
};

Window_FormationGrid.prototype.windowWidth = function() {
	return Graphics.boxWidth;
};

Window_FormationGrid.prototype.windowHeight = function() {
	return Graphics.boxHeight;
};

Window_FormationGrid.prototype.maxCols = function() {
	return maxActorGridColumns;
};

Window_FormationGrid.prototype.numVisibleRows = function() {
	return maxActorGridRows;
};

Window_FormationGrid.prototype.maxItems = function() {
	return Math.min(Math.max(this.numVisibleRows() * this.maxCols() + 1, $gameParty.highestGridIndex() + 2), $gameParty.maxBattleMembers() + $gameParty.size() - 1);
};

Window_FormationGrid.prototype.spacing = function() {
	return 0;
};

Window_FormationGrid.prototype.itemHeight = function() {
	return 144;
};

Window_FormationGrid.prototype.itemWidth = function() {
	return ~~((this.windowWidth() - this.padding * 2 + this.spacing()) / this.maxCols() - this.spacing());
};

Window_FormationGrid.prototype.itemRect = function(gridIndex) {
	var rect = new Rectangle();
	rect.width = this.itemWidth();
	rect.height = this.itemHeight();
	rect.x = (gridIndex % this.maxCols()) * (rect.width + this.spacing()) - this._scrollX;
	rect.y = ~~(gridIndex / this.maxCols()) * rect.height - this._scrollY;
	return rect;
};

Window_FormationGrid.prototype.drawBorders = function(index) {
	var rect = this.itemRect(index);

	// draw borders
	this.contents.fillRect(rect.x, rect.y, 2, rect.height);
	this.contents.fillRect(rect.x, rect.y, rect.width, 2);
	this.contents.fillRect(rect.x+rect.width-1, rect.y, 2, rect.height);
	this.contents.fillRect(rect.x, rect.y+rect.height-2, rect.width, 2);
};

Window_FormationGrid.prototype.drawActorStats = function(actor) {
	if (!actor) return;
	var gridIndex = actor.gridIndex();
	var col = actor.gridColumn(this.maxCols());
	var row = actor.gridRow(this.numVisibleRows());
	var width = this.itemWidth();
	var height = this.itemHeight();
	var x = width * col;
	var y = height * row;
	var rect = this.itemRect(gridIndex);

	// draw name
	this.drawText(actor.name(), rect.x + 128 + this.contents.fontSize, rect.y, width, 'left');

	// draw face
	this.drawActorFace(actor, rect.x + (this.contents.fontSize >> 1), rect.y + 2, 128, rect.height - 4);

	// draw gauge bars
	this.drawGaugeArea(rect, actor);

};

Window_FormationGrid.prototype.drawGaugeArea = function(rect, actor) {
	// horizontal gauges
	var fontSize = this.contents.fontSize;
	var x = rect.x + 128 + fontSize;
	var y = rect.y + fontSize;
	var dy = this.itemHeight() >> 2;
	var gaugeWidth = rect.x + rect.width - x - 10;
	this.drawActorHp(actor, x, y, gaugeWidth);
	this.drawActorMp(actor, x, y + dy, gaugeWidth);
	this.drawActorTp(actor, x, y + dy * 2, gaugeWidth);
};

Window_FormationGrid.prototype.drawItem = function(index) {
	this.drawItemBackground(index);
	this.drawBorders(index);
	this.drawActorStats($gameParty.gridIndexActor(index));
};

Window_FormationGrid.prototype.drawItemBackground = Window_MenuStatus.prototype.drawItemBackground;

Window_FormationGrid.prototype.pendingIndex = function() {
    return this._pendingIndex;
};

Window_FormationGrid.prototype.setPendingIndex = function(index) {
    var lastPendingIndex = this._pendingIndex;
    this._pendingIndex = index;
    this.redrawItem(this._pendingIndex);
    this.redrawItem(lastPendingIndex);
};

//Window_FormationGrid.prototype.refresh = function() {
	// update window contents

//	this.drawPartyStats();
//};

})();


